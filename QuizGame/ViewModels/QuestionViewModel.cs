﻿using QuizGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizGame.ViewModels
{
    public class QuestionViewModel
    {
        public QuestionViewModel()
        {
            QuizMode = QuizSettings.DefaultQuizMode().ToString();
        }

        public Question NextQuestion { get; set; }

        public string Text { get; set; }

        public string QuizMode { get; set; }

        List<Answer> _answers;

        public List<Answer> Answers
        {
            get { return _answers; }
            set
            {
                _answers = value;
                SetRandomAnswer();
            }
        }

        public string Question
        {
            get
            {
                return "Who said it?";
            }
        }

        public Answer RandomAnswer { get; set; }

        public bool IsLast
        {
            get
           {
                return NextQuestion == null;
            }
        }

        private void SetRandomAnswer()
        {
            var random = new Random();
            int i = random.Next(Answers.Count);
            RandomAnswer = Answers[i];
        }
    }
}