﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuizGame.Models;

namespace QuizGame.Controllers
{
    public class AnswersController : Controller
    {
        private QuizGameContext db = new QuizGameContext();
        
        public ActionResult Validate(int id, bool consideredTrue = true)
        {
            var answer = db.Answers.Find(id);
            if(answer == null)
            {
                return new HttpNotFoundResult();
            }

            var correctAnswer = answer.Text;
            if (!answer.Correct)
            {
                correctAnswer = answer.Question.Answers.Where(a => a.Correct).FirstOrDefault()?.Text;
            }

            var guessedCorrectly = answer.Correct == consideredTrue;

            var result = new {
                correct = answer.Correct,
                guessedCorrectly = guessedCorrectly,
                message = guessedCorrectly ? "Correct! The right answer is: …" : "Sorry, you are wrong! The right answer is: …",
                correctAnswer = correctAnswer
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
