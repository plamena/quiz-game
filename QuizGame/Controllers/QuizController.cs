﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuizGame.Models;

namespace QuizGame.Controllers
{
    public class QuizController : Controller
    {
        public ActionResult Settings()
        {
            if(Session["QuizMode"] == null)
            {
                Session["QuizMode"] = QuizSettings.DefaultQuizMode().ToString();
            }

            ViewBag.QuizMode = Session["QuizMode"];

            return View();
        }

        [HttpPost]
        public ActionResult Settings(string Mode)
        {
            if(Enum.IsDefined(typeof(QuizMode), Mode))
            {
                Session["QuizMode"] = Mode;
            }
            
            return RedirectToAction("Details", "Questions");
        }
    }
}