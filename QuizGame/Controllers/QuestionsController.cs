﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QuizGame.Models;
using QuizGame.ViewModels;

namespace QuizGame.Controllers
{
    public class QuestionsController : Controller
    {
        private QuizGameContext db = new QuizGameContext();

        // GET: Questions/Details/5
        public ActionResult Details(int? id)
        {
            Question question = null;
            if (id == null)
            {
                question = db.Questions.First();
            }
            else
            {
                question = db.Questions.Find(id);
            }

            if (question == null)
            {
                return HttpNotFound();
            }

            var quizMode = Session["QuizMode"];

            var nextQuestion =  db.Questions.Where(q => q.Id > question.Id).FirstOrDefault();

            var vm = new QuestionViewModel() { NextQuestion = nextQuestion, Text = question.Text, Answers = question.Answers.ToList() };

            if (quizMode != null)
            {
                vm.QuizMode = quizMode.ToString();
            }

            return View(vm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
