﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QuizGame.Models
{
    public class Question
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "ntext")]
        public string Text { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }
    }
}