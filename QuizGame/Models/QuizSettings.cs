﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizGame.Models
{
    static class QuizSettings
    {
        public static QuizMode DefaultQuizMode()
        {
            return QuizMode.Binary;
        }
    }
}