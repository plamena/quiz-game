﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QuizGame.Models
{
    public class Answer
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int QuestionId { get; set; }

        public virtual Question Question { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Correct { get; set; }
    }
}