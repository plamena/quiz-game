﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizGame.Models
{
    public enum QuizMode
    {
        Binary = 1,
        MultipleChoice = 2
    }
}