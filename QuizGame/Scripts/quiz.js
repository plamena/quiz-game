﻿$(document).ready(function () {

    if ($(".js-quiz-mode").length) {
        var quizMode = $(".js-quiz-mode").data("quiz-mode");
        if (quizMode == "MultipleChoice") {
            $(".js-binary-mode").hide();
            $(".js-multiple-choice-mode").show();
        }
    }

    $(".js-answer-yes").click(function () {
        var answerId = $(".js-suggested-answer").data("answer-id");
        validateAnswer(answerId, "true");
    });

    $(".js-answer-no").click(function () {
        var answerId = $(".js-suggested-answer").data("answer-id");
        validateAnswer(answerId, "false");
    });

    $(".js-answer-mutiple-choice").click(function () {
        var answerId = $(this).data("answer-id");
        validateAnswer(answerId, "true");
    });

    function validateAnswer(answerId, consideredTrue) {
        $.ajax({
            url: "/answers/validate/" + answerId,
            data: { consideredTrue: consideredTrue }
        }).done(function (data) {
            console.log(data);

            $(".js-multiple-choice-mode").hide();
            $(".js-binary-mode").hide();

            $(".js-quiz-result").text(data["message"]);
            $(".js-quiz-result").show();

            $(".js-quiz-correct-answer").text(data["correctAnswer"]);
            $(".js-quiz-correct-answer").show(1000);

            $(".js-next-question").show();
        });
    }
});

