namespace QuizGame.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<QuizGame.Models.QuizGameContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(QuizGameContext context)
        {
            SeedQuestions(context);
            SeedAnswers(context);
        }

        private void SeedQuestions(QuizGameContext context)
        {
            context.Questions.AddOrUpdate(
              q => q.Id,
              new Question { Id = 1, Text = "Anyone who has never made a mistake has never tried anything new." },
              new Question { Id = 2, Text = "A penny saved is a penny earned." },
              new Question { Id = 3, Text = "If you can't feed a hundred people, then feed just one." },
              new Question { Id = 4, Text = "Ask not what your country can do for you. Ask what you can do for your country." },
              new Question { Id = 5, Text = "You must be the change you wish to see in the world." },
              new Question { Id = 6, Text = "It has been said that democracy is the worst form of Government except for all those other forms that have been tried." },
              new Question { Id = 7, Text = "A dreamer is one who can only find his way by moonlight, and his punishment is that he sees the dawn before the rest of the world." }
            ) ;
        }

        private void SeedAnswers(QuizGameContext context)
        {
            context.Answers.AddOrUpdate(
              a => a.Id,
              new Answer { Id = 1, QuestionId = 1, Text = "Albert Einstein", Correct = true },
              new Answer { Id = 2, QuestionId = 1, Text = "Mohandas Gandhi", Correct = false },
              new Answer { Id = 3, QuestionId = 1, Text = "Rene Descartes", Correct = false },

              new Answer { Id = 4, QuestionId = 2, Text = "Benjamin Franklin", Correct = true },
              new Answer { Id = 5, QuestionId = 2, Text = "Sir Winston Churchill", Correct = false },
              new Answer { Id = 6, QuestionId = 2, Text = "John F.Kennedy", Correct = false },

              new Answer { Id = 7, QuestionId = 3, Text = "Mohandas Gandhi", Correct = false },
              new Answer { Id = 8, QuestionId = 3, Text = "Oscar Wilde", Correct = false },
              new Answer { Id = 9, QuestionId = 3, Text = "Mother Teresa", Correct = true },

              new Answer { Id = 10, QuestionId = 4, Text = "John F.Kennedy", Correct = true },
              new Answer { Id = 11, QuestionId = 4, Text = "Hector Berlioz", Correct = false },
              new Answer { Id = 12, QuestionId = 4, Text = "Oscar Wilde", Correct = false },

              new Answer { Id = 13, QuestionId = 5, Text = "Sophocles", Correct = false },
              new Answer { Id = 14, QuestionId = 5, Text = "Mohandas Gandhi", Correct = true },
              new Answer { Id = 15, QuestionId = 5, Text = "Ralph Waldo Emerson", Correct = false },

              new Answer { Id = 16, QuestionId = 6, Text = "Helen Keller", Correct = false },
              new Answer { Id = 17, QuestionId = 6, Text = "Oscar Wilde", Correct = false },
              new Answer { Id = 18, QuestionId = 6, Text = "Sir Winston Churchill", Correct = true },

              new Answer { Id = 19, QuestionId = 7, Text = "Oscar Wilde", Correct = true },
              new Answer { Id = 20, QuestionId = 7, Text = "Rene Descartes", Correct = false },
              new Answer { Id = 21, QuestionId = 7, Text = "John F.Kennedy", Correct = false }
            );
        }
    }
}
