namespace QuizGame.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAnswers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    QuestionId = c.Int(nullable: false),
                    Text = c.String(nullable: false),
                    Correct = c.Boolean(nullable: false, defaultValue: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId);

            Sql("create unique nonclustered index AK_CorrectAnswer on dbo.Answers(Correct, QuestionId) where Correct = 1");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.Answers");
        }
    }
}
