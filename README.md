# Famous Quote Quiz

## Project details
ASP.NET MVC Application with Code first and LocalDB

## Project set up
1. build the project
2. run update-database

## Description
There are two entities - questions and answers. Each question can have many answers but only can be correct ensured by a filtered unique index.

The application starts with the details of the first question. The user's answer is validated with a jquery call to Answers/Validate. After the user subsmits his answer the button "Next" appears which leads to the next question or the button "Start again the quiz" if it's the last question.

The question details view is a strongly typed view with model QuestionViewModel (ViewModels/QuestionViewModel).

Difficulty mode - It is stored in the user's session. Managed by QuizController.


Css file - Quiz.css

Javascript file - Quiz.js.